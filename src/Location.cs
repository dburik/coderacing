namespace Com.CodeGame.CodeRacing2015.DevKit.CSharpCgdk
{
    using System;

    internal struct Location : IEquatable<Location>
    {
        private readonly int x;

        private readonly int y;

        public Location(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int X
        {
            get { return this.x; }
        }

        public int Y
        {
            get { return this.y; }
        }

        public bool Equals(Location other)
        {
            return this.x == other.x &&
                   this.y == other.y;
        }

        public static bool operator ==(Location location1, Location location2)
        {
            return location1.Equals(location2);
        }

        public static bool operator !=(Location location1, Location location2)
        {
            return !(location1 == location2);
        }
    }
}