﻿namespace Com.CodeGame.CodeRacing2015.DevKit.CSharpCgdk
{
    internal struct Rectangle
    {
        private readonly double x;

        private readonly double y;

        private readonly double width;

        private readonly double heigth;

        public Rectangle(double x, double y, double width, double heigth)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.heigth = heigth;
        }

        public double X
        {
            get
            {
                return this.x;
            }
        }

        public double Y
        {
            get
            {
                return this.y;
            }
        }

        public double Width
        {
            get
            {
                return this.width;
            }
        }

        public double Heigth
        {
            get
            {
                return this.heigth;
            }
        }

        public bool Contains(Point point)
        {
            return this.Contains(point.X, point.Y);
        }

        public bool Contains(double x, double y)
        {
            return this.x <= x &&
                   x < this.x + this.width &&
                   this.y <= y &&
                   y < this.y + this.heigth;
        }
    }
}