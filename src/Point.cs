namespace Com.CodeGame.CodeRacing2015.DevKit.CSharpCgdk
{
    using System;

    internal struct Point : IEquatable<Point>
    {
        private readonly double x;

        private readonly double y;

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double X
        {
            get
            {
                return this.x;
            }
        }

        public double Y
        {
            get
            {
                return this.y;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            return obj is Point && this.Equals((Point)obj);
        }

        public override int GetHashCode()
        {
            unchecked { return (this.x.GetHashCode() * 397) ^ this.y.GetHashCode(); }
        }

        public bool Equals(Point other)
        {
            return this.x.Equals(other.x) && this.y.Equals(other.y);
        }

        public static bool operator ==(Point location1, Point location2)
        {
            return location1.Equals(location2);
        }

        public static bool operator !=(Point location1, Point location2)
        {
            return !(location1 == location2);
        }
    }
}