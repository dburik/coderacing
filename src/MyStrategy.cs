namespace Com.CodeGame.CodeRacing2015.DevKit.CSharpCgdk
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Com.CodeGame.CodeRacing2015.DevKit.CSharpCgdk.Model;

    public sealed class MyStrategy : IStrategy
    {
        private const int TileSize = 800;

        private bool flag = true;

        private IList<Location> path;

        public void Move(Car self, World world, Game game, Move move)
        {
            var tileSize = game.TrackTileSize;

            var currentTileX = (int)(self.X / tileSize);
            var currentTileY = (int)(self.Y / tileSize);

            var currentTileLocation = new Location(currentTileX, currentTileY);

            if (this.path == null || !this.path.Any())
            {
                this.path = Pithfinder.FindPath(world.TilesXY, currentTileLocation, new Location(self.NextWaypointX, self.NextWaypointY));

                Console.WriteLine("Building Path");
                PrintPath(this.path);
            }

            var nextTileLocation = this.path.First();

            var nextTileLeftTopX = nextTileLocation.X * tileSize;
            var nextTileLeftTopY = nextTileLocation.Y * tileSize;

            var nextTileRect = new Rectangle(nextTileLeftTopX, nextTileLeftTopY, tileSize, tileSize);

            if (nextTileRect.Contains(self.X, self.Y))
            {
                this.path = this.path.Skip(1).ToList();
            }

            var nextWaypointX = (nextTileLocation.X + 0.5D) * tileSize;
            var nextWaypointY = (nextTileLocation.Y + 0.5D) * tileSize;

            var cornerTileOffset = 0.2D * tileSize;

            var nextTileType = world.TilesXY[nextTileLocation.X][nextTileLocation.Y];

            switch (nextTileType)
            {
                case TileType.LeftTopCorner:
                    nextWaypointX += cornerTileOffset;
                    nextWaypointY += cornerTileOffset;
                    break;
                case TileType.RightTopCorner:
                    nextWaypointX -= cornerTileOffset;
                    nextWaypointY += cornerTileOffset;
                    break;
                case TileType.LeftBottomCorner:
                    nextWaypointX += cornerTileOffset;
                    nextWaypointY -= cornerTileOffset;
                    break;
                case TileType.RightBottomCorner:
                    nextWaypointX -= cornerTileOffset;
                    nextWaypointY -= cornerTileOffset;
                    break;
            }

            var angleToWaypoint = self.GetAngleTo(nextWaypointX, nextWaypointY);
            var speedModule = Math.Sqrt(Math.Pow(self.SpeedX, 2) + Math.Pow(self.SpeedY, 2));

            move.WheelTurn = angleToWaypoint * 70.0D / Math.PI;
            move.EnginePower = 0.9;

            if (speedModule * speedModule * Math.Abs(angleToWaypoint) > Math.Pow(2.3, 2) * Math.PI)
            {
                move.IsBrake = true;
            }

            if (world.Tick % 100 == 0)
            {
                Console.WriteLine("DURATION {0}", game.TickCount);
                Console.WriteLine("TICK {0}", world.Tick);
                Console.WriteLine("Tile [{0}][{1}]", currentTileX, currentTileY);
                Console.WriteLine("Engine: {0} | Wheel Turn: {1} | Angle: {2}", self.EnginePower, self.WheelTurn, self.Angle, self.Durability);
                Console.WriteLine();
            }
        }

        private void PrintPath(IEnumerable<Location> path)
        {
            foreach (var location in path)
            {
                Console.Write("-> [{0},{1}]", location.X, location.Y);
            }
        }
    }
}