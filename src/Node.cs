namespace Com.CodeGame.CodeRacing2015.DevKit.CSharpCgdk
{
    internal class Node
    {
        public Location Location { get; set; }

        public Node PreviousNode { get; set; }

        public int G { get; set; }

        public int H { get; set; }

        public int F
        {
            get { return this.G + this.H; }
        }
    }
}