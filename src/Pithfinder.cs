﻿namespace Com.CodeGame.CodeRacing2015.DevKit.CSharpCgdk
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using Com.CodeGame.CodeRacing2015.DevKit.CSharpCgdk.Model;

    internal class Pithfinder
    {
        public static IList<Location> FindPath(TileType[][] map, Location start, Location goal)
        {
            var closedSet = new Collection<Node>();
            var openSet = new Collection<Node>();

            var startNode = new Node
                {
                    Location = start, 
                    PreviousNode = null, 
                    G = 0, 
                    H = CalculateH(start, goal)
                };

            openSet.Add(startNode);

            while (openSet.Any())
            {
                var currentNode = openSet.OrderBy(node => node.F).First();

                if (currentNode.Location == goal)
                {
                    return ReconstructPath(currentNode);
                }

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                foreach (var neighbourNode in GetNeighbours(currentNode, goal, map))
                {
                    if (closedSet.Select(node => node.Location).Contains(neighbourNode.Location))
                    {
                        continue;
                    }

                    if (openSet.Select(node => node.Location).Contains(neighbourNode.Location))
                    {
                        var openNode = openSet.First(node => node.Location == neighbourNode.Location);

                        if (openNode.G > neighbourNode.G)
                        {
                            openNode.G = neighbourNode.G;
                            openNode.PreviousNode = currentNode;
                        }
                    }
                    else
                    {
                        openSet.Add(neighbourNode);
                    }
                }
            }

            return null;
        }

        private static IList<Location> ReconstructPath(Node node)
        {
            var pathBack = new Collection<Location>();

            while (node.PreviousNode != null)
            {
                pathBack.Add(node.Location);
                node = node.PreviousNode;
            }

            return pathBack.Reverse().ToArray();
        }

        private static IEnumerable<Node> GetNeighbours(Node currentNode, Location goal, TileType[][] map)
        {
            var neighbours = new Collection<Node>();

            var possibleLocations = GetPossibleNextLocations(currentNode.Location, map);

            foreach (var possibleLocation in possibleLocations)
            {
                // if (possibleLocation.X < 0 || possibleLocation.X > map.GetLength(0))
                // continue;

                // if (possibleLocation.Y < 0 || possibleLocation.Y > map.GetLength(1))
                // continue;
                neighbours.Add(
                    new Node
                        {
                            Location = possibleLocation, 
                            PreviousNode = currentNode, 
                            G = GetDistanceBetweenNeighbours(), 
                            H = CalculateH(possibleLocation, goal)
                        });
            }

            return neighbours;
        }

        private static int GetDistanceBetweenNeighbours()
        {
            return 1;
        }

        private static IEnumerable<Location> GetPossibleNextLocations(Location currentLocation, TileType[][] map)
        {
            var left = new Location(currentLocation.X - 1, currentLocation.Y);
            var right = new Location(currentLocation.X + 1, currentLocation.Y);
            var top = new Location(currentLocation.X, currentLocation.Y - 1);
            var bottom = new Location(currentLocation.X, currentLocation.Y + 1);

            var tileType = map[currentLocation.X][currentLocation.Y];

            switch (tileType)
            {
                case TileType.Horizontal:
                    return new[] { left, right };

                case TileType.Vertical:
                    return new[] { top, bottom };

                case TileType.LeftTopCorner:
                    return new[] { right, bottom };

                case TileType.RightTopCorner:
                    return new[] { left, bottom };

                case TileType.RightBottomCorner:
                    return new[] { left, top };

                case TileType.LeftBottomCorner:
                    return new[] { right, top };

                case TileType.TopHeadedT:
                    return new[] { left, right, bottom };

                case TileType.RightHeadedT:
                    return new[] { top, bottom, left };

                case TileType.BottomHeadedT:
                    return new[] { top, right, left };

                case TileType.LeftHeadedT:
                    return new[] { top, right, bottom };

                case TileType.Crossroads:
                    return new[] { top, right, bottom, left };

                default:
                    throw new NotImplementedException("Unknown TileType");
            }
        }

        private static int CalculateH(Location from, Location to)
        {
            return Math.Abs(to.X - from.X) + Math.Abs(to.Y - from.Y);
        }
    }
}